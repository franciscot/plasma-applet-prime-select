# Translation of plasmoid-prime-select in LANGUAGE
# Copyright (C) 2021
# This file is distributed under the same license as the plasmoid-prime-select package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: plasmoid-prime-select\n"
"Report-Msgid-Bugs-To: https://gitlab.com/franciscot/plasmoid-prime-select\n"
"POT-Creation-Date: 2021-03-15 21:31+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../metadata.desktop
msgid "Plasma Prime-select"
msgstr ""

#: ../metadata.desktop
msgid "Switch your integrated or discrete graphic card using prime-select!"
msgstr ""

#: ../contents/config/config.qml
msgid "General"
msgstr ""

#: ../contents/ui/configGeneral.qml
msgid "Use integrated mode:"
msgstr ""

#: ../contents/ui/configGeneral.qml
msgid "Intel only"
msgstr ""

#: ../contents/ui/configGeneral.qml
msgid "Hybrid"
msgstr ""

#: ../contents/ui/main.qml
msgid "You will now be prompted to enter the superuser password to switch graphics mode. A few seconds after applying the changes you will be asked to restart. Do you wish to continue?"
msgstr ""

#: ../contents/ui/main.qml
msgid "%1 currently in use"
msgstr ""

#: ../contents/ui/main.qml
msgid "Prime is not woriking"
msgstr ""

#: ../contents/ui/main.qml
msgid "Switch"
msgstr ""
